angular.module('kronos')

.factory('Plan', function($http) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data


  return {
    getAllPlansOfUser: function(userId) {
      return $http.get('http://www.plisplasfood.com/api/get_user_plans.php/?userid='+userId).then(function(res){
        return res;
      },function(err){
          return err;
      })
    },
    edit: function(plan) {
      $http.post('http://www.plisplasfood.com/api/edit_plan.php/',plan).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    },
    get: function(planId) {
      return $http.get('http://www.plisplasfood.com/api/get_plan.php/?id='+planId).then(function(res){
        return res;
      },function(err){
          return err;
      })
    },
    add: function(plan) {
      $http.post('http://www.plisplasfood.com/api/insert_plan.php',plan).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    }
  };
});
