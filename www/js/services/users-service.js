angular.module('kronos')

.factory('Users', function($http) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
var user = [];

  return {
    getCurrentUser: function(){
      return user;
    },
    setCurrentUser: function(u){
      user = u;
    },
    edit: function(users) {
      $http.post('http://www.plisplasfood.com/api/edit_users.php/',users).then(function(res){
        return res;
      }, function(err){
          return err;
      })
    },
    get: function(users) {
      //console.log(users);
      return $http.get('http://www.plisplasfood.com/api/get_users.php/?users_username='+users['users_username']+'&users_password='+users['users_password']).then(function(res){
        //console.log(res);
        return res.data;
      }, function(err){
          return err;
      })
    },
    add: function(users) {
      return $http.get('http://www.plisplasfood.com/api/insert_users.php?users_username='+users.users_username+'&users_email='+users.users_email+'&users_password='+users.users_password).then(function(res){
        return res;
      },function(err){
          return err;
      })
    }
  };
});
