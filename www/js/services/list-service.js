angular.module('kronos')

.factory('List', function($http) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data


  return {

    edit: function(list) {
      $http.post('http://www.plisplasfood.com/api/edit_list.php/',list).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    },
    get: function(mealId) {
      $http.get('http://www.plisplasfood.com/api/get_list.php/?id='+mealId).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    },
    add: function(list) {
      $http.post('http://www.plisplasfood.com/api/insert_list.php',list).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    }
  };
});
