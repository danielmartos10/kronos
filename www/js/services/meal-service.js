angular.module('kronos')

.factory('Meal', function($http) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data


  return {

    edit: function(meal) {
      $http.post('http://www.plisplasfood.com/api/edit_meal.php/',meal).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    },
    get: function(planId) {
      $http.get('http://www.plisplasfood.com/api/get_meal.php/?id='+planId).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    },
    add: function(meal) {
      $http.post('http://www.plisplasfood.com/api/insert_meal.php',meal).then(function(res){
        return res;
      }).error(function(err){
          return err;
      })
    }
  };
});
