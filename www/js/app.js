// Ionic kronos App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'kronos' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'kronos.services' is found in services.js
// 'kronos.controllers' is found in controllers.js
angular.module('kronos', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginController'
  })

    .state('register', {
    url: '/register',
    templateUrl: 'templates/register.html',
    controller: 'RegisterController'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('home', {
      url: '/home',
      templateUrl: 'templates/home-first.html',
      controller: 'HomeController'

    })

    .state('planning', {
        url: '/planning',
        templateUrl: 'templates/planning.html',
        controller: 'PlanController'

      })
    .state('tab.planning-first', {
        url: '/planning-first',
        views: {
          'planning-first':{
            templateUrl: 'templates/planning-first.html',
            controller: 'PlanController'
          }
        }
    })
    .state('tab.express-menu', {
        url: '/express-menu',
        views: {
          'express-menu':{
            templateUrl: 'templates/express-menu.html',
            controller: 'ExpressController'
          }
        }
    })
    .state('tab.user', {
        url: '/user',
        views: {
          'user':{
            templateUrl: 'templates/user.html',
            controller: 'UserController'
          }
        }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })



  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
