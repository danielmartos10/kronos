angular.module('kronos')
.controller('LoginController', function($rootScope,$scope,$state,Users) {
  console.log('login');
  $scope.user = [];
  $scope.loginFacebook = function() {
    // facebookConnectPlugin.login(["scopes"], function(success){}, function(error){})
    facebookConnectPlugin.login( ["email","public_profile","user_friends"],
    function(result){
      //console.log(result);
      facebookConnectPlugin.api("/me?fields=id,name,about,age_range,email",
    ["public_profile", "user_friends", "email"],
    function(res){
      console.log(res);
      //Save/Check if user exists
      $state.go("tab.dash");
    }, function(err){
      console.log(err);
    });
    // setTimeout(function(){
    //   console.log('GO');
    // }, 500);
    }, function(error){
      console.log(error);
    });
  };
  $scope.loginForm = function(){
    console.log('Trying login');
    console.log($scope);
    Users.get($scope.user).then(function(res){
      if (res[0].error){
        $scope.error = res[0].error;
      }else{
        Users.setCurrentUser(res[0]);
        console.log($scope.user);
        $state.go("home");
      };
    });
  };
});
