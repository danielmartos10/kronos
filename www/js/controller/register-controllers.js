angular.module('kronos')

.controller('RegisterController', function($scope,$state,Users) {
  $scope.user={};
  console.log($scope);
  $scope.avatar = function(opcio){
    $scope.user.users_icon = opcio;

    var avatar1 = document.getElementById('avatar1');
    var avatar2 = document.getElementById('avatar2');
    if (opcio == 1){
      avatar1.classList.add("avatar-active");
      avatar2.classList.remove("avatar-active");
      Users.add($scope.user);
    }
    if (opcio == 2){
      avatar2.classList.add("avatar-active");
      avatar1.classList.remove("avatar-active");
      Users.add($scope.user);
    }
  };
  $scope.registerForm = function(){

      console.log('Trying Register');

      if ($scope.user.users_password == $scope.user.users_repeatpassword) {
        Users.add($scope.user).then(function(res){
              console.log(res);
              if (res.data[0].success==true){
                  $state.go("home");
              }else{
                  $scope.error='This Username Already Exists';
              }
        });
      }else{
          $scope.error ='Password Doesn\'t Match';
      }

  };
});
