angular.module('kronos')

.controller('DashCtrl', function($scope) {})

.controller('LoginController', function($scope,$state) {
  $scope.loginFacebook = function() {
    // facebookConnectPlugin.login(["scopes"], function(success){}, function(error){})
    facebookConnectPlugin.login( ["email","public_profile","user_friends"],
    function(result){
      //console.log(result);
      facebookConnectPlugin.api("/me?fields=id,name,about,age_range,email",
    ["public_profile", "user_friends", "email"],
    function(res){
      console.log(res);
      //Save/Check if user exists
      $state.go("tab.dash");
    }, function(err){
      console.log(err);
    });
    // setTimeout(function(){
    //   console.log('GO');
    // }, 500);
    }, function(error){
      console.log(error);
    });
  };
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
